#pragma once

#include <Arduino.h>
#include "Event.h"


Event::Event(uint32_t duration, const char* name, bool indefinite) : 
    duration(duration), 
    nextEvent(NULL),
    indefinite(indefinite),
    name(name)
    {};
