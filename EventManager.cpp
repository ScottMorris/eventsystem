#pragma once 

#include <Arduino.h>
#include "EventManager.h"

EventManager::EventManager() : firstEvent(NULL), lastEvent(NULL) {};

void EventManager::registerEvent(Event* event) {
    uint32_t currentMillis = millis();
    event->millisStamp = currentMillis;
    pushEvent(event);
    event->Start();
}

bool EventManager::pushEvent(Event* event) {
    if(firstEvent == NULL) {
        firstEvent = event;
        lastEvent = event;
    } else {
        lastEvent->nextEvent = event;
        lastEvent = event;
    }
}

bool EventManager::popEvent(Event* event) {
    Event* currentEvent = firstEvent;
    Event* previousEvent = NULL;
    while (currentEvent != NULL) {
        Event* nextEvent = currentEvent->nextEvent;
        if(currentEvent == event) {
            if(previousEvent == NULL) { // First event
                firstEvent = nextEvent;
                if(nextEvent == NULL) { // Nothing else in the list
                    lastEvent = firstEvent;
                }
            } else { // Other elements
                previousEvent->nextEvent = nextEvent;
                currentEvent->nextEvent = NULL;
                if(nextEvent == NULL) {
                    lastEvent = previousEvent;
                }
            }
            break;
        }
        previousEvent = currentEvent;
        currentEvent = currentEvent->nextEvent;
    }
}


void EventManager::runEvents() {
    uint32_t currentMillis = millis();
    Event* currentEvent = firstEvent;
    while (currentEvent != NULL) {
        if (currentMillis - currentEvent->millisStamp >= currentEvent->duration) {
            currentEvent->onRun();
            if(!currentEvent->indefinite)
                popEvent(currentEvent);
            else 
                currentEvent->millisStamp = millis();
        }
         currentEvent = currentEvent->nextEvent;
    }
}