#pragma once 
#include "EventDataManager.h"

EventDataManager::EventDataManager() : 
    firstDatum(NULL), lastDatum(NULL) 
    {};

void EventDataManager::addDatum(Datum* datum, const char* tag) {
    pushDatum(datum);
}

// void EventDataManager::readData(void (*mapper)(Datum*, int)) {
//     Datum* currentDatum = firstDatum;
//     int i = 0;
//     while (currentDatum != NULL) {
//         mapper(currentDatum, i);
//         currentDatum = currentDatum->nextDatum;
//         i++;
//     }
// }

void EventDataManager::readData(std::function<void(Datum**, int)> func) {
    Datum* data[size];
    int i = 0;
    Datum* currentDatum = firstDatum;
    while (currentDatum != NULL) {
        data[i] = currentDatum;
        currentDatum = currentDatum->nextDatum;
        i++;
    }
    func(data, size);
}

bool EventDataManager::popDatum(Datum* datum) {
    Datum* currentDatum = firstDatum;
    Datum* previousDatum = NULL;
    while (currentDatum != NULL) {
        Datum* nextDatum = currentDatum->nextDatum;
        if(currentDatum == datum) {
            if(previousDatum == NULL) { // First Datum
                firstDatum = nextDatum;
                if(nextDatum == NULL) { // Nothing else in the list
                    lastDatum = firstDatum;
                }
            } else { // Other elements
                previousDatum->nextDatum = nextDatum;
                currentDatum->nextDatum = NULL;
                if(nextDatum == NULL) {
                    lastDatum = previousDatum;
                }
            }
            break;
        }
        
        previousDatum = currentDatum;
        currentDatum = currentDatum->nextDatum; 
    }
    size--;
    delete currentDatum;
}

bool EventDataManager::pushDatum(Datum* datum) {
    if(firstDatum == NULL) {
        firstDatum = datum;
        lastDatum = datum;
    } else {
        lastDatum->nextDatum = datum;
        lastDatum = datum;
    }
    size++;
} 

int EventDataManager::getSize() {
    return size;
}

bool EventDataManager::clear() {
    Datum* currentDatum = firstDatum;
    while (currentDatum != NULL) {
        currentDatum = currentDatum->nextDatum;
        popDatum(currentDatum);
    }
}