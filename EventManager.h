#pragma once

#include "Event.h"

class EventManager {
    public:
        EventManager();
        void registerEvent(Event* event);
        void runEvents();
    private:
        bool popEvent(Event* event);
        bool pushEvent(Event* event); 

        Event* firstEvent;
        Event* lastEvent;
};