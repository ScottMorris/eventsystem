#pragma once
#include "Datum.h"

Datum::Datum(DynamicJsonDocument* value, int size, const char* tag) :
    value(value),
    size(size),
    tag(tag),
    nextDatum(NULL)
    {};