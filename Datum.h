#pragma once
#include <ArduinoJson.h>

class Datum {
	public:
		Datum(DynamicJsonDocument* value, int size, const char* tag);
		const char* tag;
		DynamicJsonDocument* value;
		int size;


	protected:
		



	private:
		friend class EventDataManager;
		Datum* nextDatum;
};