#pragma once

class Event {
    public:
        Event(uint32_t duration, const char* name = NULL, bool indefinite = false);
        const char* name;

    protected:
        virtual bool onStart() { return true; };
        virtual void onRun() {};
        bool indefinite;


    private:
        friend class EventManager;
        Event* nextEvent;
        uint32_t duration;
        uint32_t millisStamp;

        bool Start() {
            return onStart();
        }
};