#pragma once

#include "Datum.h"
#include <functional>

class EventDataManager {
    public:
        EventDataManager();
        void addDatum(Datum* datum, const char* tag = "");
        // void readData(void (*mapper)(Datum* datum, int index));
        void readData(std::function<void(Datum**, int)> func);
        int getSize();
        bool clear();
    private:
        bool popDatum(Datum* datum);
        bool pushDatum(Datum* datum);
        int size = 0;

        Datum* firstDatum;
        Datum* lastDatum;
};